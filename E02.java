import java.time.LocalDate;
import java.util.Calendar;



class Fecha {
    // Clase que representa una fechagt
    private int dia;
    private int mes;
    private int anyo;

    private int getDia() {
        return this.dia;
    }

    private void setDia(final int dia) {
        this.dia = dia;
    }

    private int getMes() {
        return this.mes;
    }

    private void setMes(final int mes) {
        this.mes = mes;
    }

    private int getAnyo() {
        return this.anyo;
    }

    private void setAnyo(final int anyo) {
        this.anyo = anyo;
    }

    public Calendar devuelve_fecha() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(this.anyo, this.mes, this.dia);
        return calendar;
    }

    public void cambia_actual(){
        
        LocalDate localDate = LocalDate.now();
        //Date date = java.sql.Date.valueOf(localDate);

        this.anyo = localDate.getYear();
        this.mes = localDate.getMonthValue();
        this.dia = localDate.getDayOfMonth();

    }
    public Long compara_fecha(Fecha... fecha){
        long miliSecondForDate1 = fecha[0].devuelve_fecha().getTimeInMillis();
        long miliSecondForDate2 = devuelve_fecha().getTimeInMillis();
        long diffInMilis =  miliSecondForDate1- miliSecondForDate2;
        long diffInDays = diffInMilis / (24 * 60 * 60 * 1000);
        long years = diffInDays/365;
        return years;
    }
    public void cambia_fecha(Integer anyo, Fecha... fecha){
        if(fecha.length > 0){
            setAnyo(fecha[0].getAnyo());
            setMes(fecha[0].getMes());
            setDia(fecha[0].getDia());
        } else if(anyo !=0) {
            setAnyo(getAnyo()+anyo);
        }

    }
    public String mostrar_fecha(){
       Calendar f = devuelve_fecha();

       return f.getTime().toString();
    }
    public Fecha(int dia, int mes, int anyo) {
        this.dia = dia;
        this.mes = mes;
        this.anyo = anyo;
    }
}




class DNI {
    // clase que representa a un DNI
    private String numero;
    private String nombre;
    private String primer_apellido;
    private String segundo_apellido;
    private Boolean sexo;
    private Fecha fecha_nacimiento;
    private Fecha fecha_expedicion;
    private Fecha fecha_vencimiento;

    public Fecha getFecha_nacimiento() {
        return this.fecha_nacimiento;
    }

    public void setFecha_nacimiento(Fecha fecha_nacimiento) {
        this.fecha_nacimiento = fecha_nacimiento;
    }

    public Fecha getFecha_expedicion() {
        return this.fecha_expedicion;
    }

    public void setFecha_expedicion(Fecha fecha_expedicion) {
        this.fecha_expedicion = fecha_expedicion;
    }

    public Fecha getfecha_vencimiento() {
        return this.fecha_vencimiento;
    }

    public void setfecha_vencimiento(Fecha fecha_vencimiento) {
        this.fecha_vencimiento = fecha_vencimiento;
    }

    public String getNumero() {
        return this.numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getNombre() {
        return this.nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Boolean getSexo() {
        return this.sexo;
    }
    public String getPrimer_apellido() {
        return this.primer_apellido;
    }

    public void setPrimer_apellido(String primer_apellido) {
        this.primer_apellido = primer_apellido;
    }

    public String getSegundo_apellido() {
        return this.segundo_apellido;
    }

    public void setSegundo_apellido(String segundo_apellido) {
        this.segundo_apellido = segundo_apellido;
    }
    public void setSexo(Boolean sexo) {
        this.sexo = sexo;
    }
    public void renovar(Fecha... fecha_parametro){
        fecha_expedicion.cambia_actual();
        Long years = fecha_nacimiento.compara_fecha(fecha_expedicion);
        if(fecha_parametro.length >0){
            //System.out.println(fecha_parametro);
            fecha_vencimiento.cambia_fecha(0,fecha_parametro[0]);
        }else{
            if(years<20){
                //System.out.println("5 años");
                fecha_vencimiento.cambia_fecha(5);
            } else if(years >=20 && years <70 ){
                //System.out.println("10 años");
                fecha_vencimiento.cambia_fecha(10);
            }else if(years>=70){
                //System.out.println("infinito");
                // Hay que estamos trabajando con CALENDAR, debemos setter bien la zona horaria y otros parametros para datos más precisos.
                Fecha infinito = new Fecha(31, 12, 9999);
                fecha_vencimiento.cambia_fecha(0, infinito);
            }
        }

    }

    public void valida_DNI(){
        // Metodo que valida y asigna un DNI en caso de ser necesario.
    }
    
    public void ver_fecha(){
        System.out.println("fecha de nacimiento: "+fecha_nacimiento.mostrar_fecha());
        System.out.println("fecha de expedicion: "+fecha_expedicion.mostrar_fecha());
        System.out.println("fecha de vencimiento: "+fecha_vencimiento.mostrar_fecha());
    }
    public DNI(String numero,String nombre,String primer_apellido,String segundo_apellido,Boolean sexo,Fecha fecha_nacimiento,Fecha fecha_expedicion,Fecha fecha_vencimiento) {
        this.nombre = nombre;
        this.primer_apellido = primer_apellido;
        this.segundo_apellido = segundo_apellido;
        this.sexo = sexo;
        this.fecha_nacimiento = fecha_nacimiento;
        this.fecha_expedicion = fecha_expedicion;
        this.fecha_vencimiento = fecha_vencimiento;
    }
}

public class E02 {
    public static void main(final String[] args) {
        Fecha nacimiento_1 =  new Fecha(12,1,1995);
        Fecha fecha2 =  new Fecha(12,1,2018);
        Fecha fecha3 =  new Fecha(12,1,2018);
        Fecha nacimiento_2 =  new Fecha(12,1,1950);

        Fecha fecha_parametro = new Fecha(30,05,2040);
        DNI dni = new DNI("weqweqweqweq","yampi", "sanchez", "gastelu", false, nacimiento_1, fecha2, fecha3);
        DNI dni2 = new DNI("weqweqweqweq2","yampi2", "sanchez2", "gastelu2", false, nacimiento_2, fecha2, fecha3);
        
        dni.renovar();
        dni.ver_fecha();

        dni2.renovar();
        dni2.ver_fecha();

        dni.renovar(fecha_parametro);
        dni.ver_fecha();
    }
}
